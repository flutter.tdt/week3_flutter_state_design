import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'image_model.dart';

class ListImage extends StatelessWidget {
  List<ImageModel> images;

  ListImage(this.images);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: images.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            padding: EdgeInsets.all(20),
            child: Image.network(images[index].url),
          );
        }
    );
  }

}