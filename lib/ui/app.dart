import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;
import 'dart:async';
import 'dart:convert';
import 'image_model.dart';
import 'list_images.dart';


class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }

}

class AppState extends State<App> {
  var counter = 0;
  List<ImageModel> imageModels = [];

  fetchImage() async {
    counter++;
    print('Fetching image no $counter...');

    var response = await get(Uri.https('jsonplaceholder.typicode.com', 'photos/$counter'));
    var imageModel = ImageModel.fromJson(json.decode(response.body));

    print('imageModel of $counter: $imageModel');
    setState(() {
      imageModels.add(imageModel);
    });

    print('counter=$counter');
    print('Size of imageModels=${imageModels.length}');
  }
  @override
  Widget build(BuildContext context) {
    var appWidget = new MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('Flutter App'),),
        body: ListImage(imageModels),
        floatingActionButton: new FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: fetchImage,
        ),
      ),
    );

    return appWidget;
  }

}
