import 'dart:convert';

import 'package:flutter/cupertino.dart';
class ImageModel {
  int id;
  String url;

  ImageModel(@required this.id, @required this.url);

  ImageModel.fromJson(@required parsedJson) {
    id = parsedJson['id'];
    url = parsedJson['url'];
  }

  String toString() {
    return '($id, $url)';
  }
}